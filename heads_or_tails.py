import random as r #importuje biblioteke random pod nazwa r


def heads_or_tails(counter): #counter to argument fucncji
    result = 0
    states = ["head", "tail"] #tablica stanow
    for result in range(counter): #petla wykonywana "counter" razy
        result = r.choice(states) #choice to metoda biblioteki random
    return(result) 


counter = int(input("Write how many coin tosses: "))
result = heads_or_tails(counter) #przypisanie funkcji heads_or_tails z argumentem counter do zmiennnej result
print(result) #wypisanie wyniku
